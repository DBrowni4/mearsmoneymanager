CREATE DATABASE MearsMoneyManager;
USE MearsMoneyManager;


CREATE TABLE Users (
    UserID int NOT NULL AUTO_INCREMENT,
    SlackID varchar(9) NOT NULL UNIQUE,
    PRIMARY KEY (UserID)
    );

CREATE TABLE Transactions (
    TransactionID int NOT NULL AUTO_INCREMENT,
    UserID int NOT NULL,
    Amount int,
    Timestamp DATETIME NOT NULL,
    PRIMARY KEY (TransactionID),
    FOREIGN KEY (UserID) REFERENCES Users(UserID)
    );

CREATE TABLE Items (
    ItemID int NOT NULL AUTO_INCREMENT,
    UserID int NOT NULL,
    Name varchar(32) UNIQUE,
    Desc varchar(128),
    PRIMARY KEY (ItemID),
    FOREIGN KEY (UserID) REFERENCES Users(UserID)
    );

CREATE TABLE Auctions (
    AuctionID int NOT NULL AUTO_INCREMENT,
    UserID int NOT NULL,
    ItemID int NOT NULL,
    Active BOOLEAN,
    Timestamp DATETIME NOT NULL,
    PRIMARY KEY (AuctionID),
    FOREIGN KEY (UserID) REFERENCES Users(UserID),
    FOREIGN KEY (ItemID) REFERENCES Items(ItemID)
    );

CREATE TABLE Bids (
    BidID int NOT NULL AUTO_INCREMENT,
    UserID int NOT NULL,
    AuctionID int NOT NULL,
    Amount int NOT NULL,
    Timestamp DATETIME NOT NULL,
    PRIMARY KEY (BidID),
    FOREIGN KEY (UserID) REFERENCES Users(UserID),
    FOREIGN KEY (AuctionID) REFERENCES Auctions(AuctionID)
    );