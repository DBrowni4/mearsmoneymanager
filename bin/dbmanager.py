import yaml
import mysql.connector

# The dbmanager class manages our SQL queries and database connection

class DBManager:
    def __init__(self, config="./config/dbconf.yaml"):
        with open(config, "r") as data:
            vals = yaml.safe_load(data)
            self.__connector = mysql.connector.connect(host=vals["host"], port=vals["port"], user=vals["username"], password=vals["password"], database="MearsMoneyManager")

    def execute(self, command):
        cursor = self.__connector.cursor()
        vals = cursor.execute(command)
        self.__connector.commit()
        return vals